// album
global.Album = require('components/album/Album').default;
global.Albums = require('components/album/Albums').default;

// artist
global.Artist = require('components/artist/Artist').default;
global.Artists = require('components/artist/Artists').default;

// song
global.Song = require('components/song/Song').default;
global.Songs = require('components/song/Songs').default;

// tab
global.Tab = require( 'components/tab/Tab' ).default;
global.Tabs = require( 'components/tab/Tabs' ).default;

// user
global.User = require('components/user/User').default;
global.Users = require('components/user/Users').default;

