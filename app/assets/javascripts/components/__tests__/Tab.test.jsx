import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import Tab from 'tab/Tab';

test('Tab renders correctly', () => {
  // render a tab
  const testTab = {
    id: 1,
    name: "Test Tab",
    user_id: 1,
    user_name: "Test User",
    average_rating: 3,
    created_at: "01/01/01"
  }
  const tree = renderer
    .create(
      <Tab  key = {testTab.id}
            tab = {testTab}
            tabsPath="/tabs"
            usersPath="/users" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});