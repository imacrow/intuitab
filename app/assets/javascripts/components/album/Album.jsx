export default class Album extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      album: this.props.album
    }
  }
  
  render () {
    const data = this.props;
    
    return (
      <tr>
        <td>
          <a href={ data.albumsPath + '/' + data.album.id }>{data.album.name}</a>
        </td>
        <td>
          <a href={ data.artistsPath + '/' + data.album.artist.id }>{data.album.artist.name}</a>
        </td>
      </tr>
    );
  }
}