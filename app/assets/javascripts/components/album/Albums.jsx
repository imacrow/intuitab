// dtHelper.initDataTables('#albums-table');
$(document).on('turbolinks:load', () => {
  if (!$(".dataTables_wrapper")[0]) {
    return $('#albums-table').DataTable();
  }
});

export default class Albums extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        albums: this.props.data
    };
  }
  
  render() {
    const data = this.props;
    
    var albums = this.state.albums.map((album, index) => {
      return <Album   key         = {album.id} 
                      album       = {album}
                      albumsPath  = {data.albumsPath} 
                      artistsPath = {data.artistsPath} />
      });
    return (
      <div className='albums'>
        <h1 className='title'>Albums ({data.data.length})</h1>
        <table className='table table-striped' id='albums-table'>
          <thead>
            <tr>
              <th>Title</th>
              <th>Artist</th>
            </tr>
          </thead>
          <tbody>
            {albums}
          </tbody>
        </table>
      </div>
    );
  }
}

Albums.defaultProps = {
    albums: []
}; 