export default class Artist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      artist: this.props.artist
    }
  }
  
  render() {
    const data = this.props;
    
    return (
      <tr>
        <td>
          <a href={ data.artistsPath + '/' + data.artist.id }>{data.artist.name}</a>
        </td>
        <td>
          todo!
        </td>
      </tr>
    );
  }
}