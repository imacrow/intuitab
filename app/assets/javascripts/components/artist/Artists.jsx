// dtHelper.initDataTables('#artists-table');
$(document).on('turbolinks:load', () => {
  if (!$(".dataTables_wrapper")[0]) {
    return $('#artists-table').DataTable();
  }
});

export default class Artists extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      artists: this.props.data
    }
  }
  
  render() {
    const data = this.props;
    
    var artists = this.state.artists.map((artist, index) => {
      return <Artist  key         = {artist.id}
                      artist      = {artist}
                      artistsPath = {data.artistsPath} />
    });
    
    return (
      <div className='artists'>
        <h1 className='title'>Artists ({data.data.length})</h1>
        <table className='table table-striped' id='artists-table'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Total Tabs</th>
            </tr>
          </thead>
          <tbody>
            {artists}
          </tbody>
        </table>
      </div>
    );
  }
}

Artists.defaultProps = {
    artists: []
}; 