export default class Song extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      song: this.props.song
    }
  }
  
  render () {
    const data = this.props;
    
    return (
      <tr>
        <td>
          <a href={ data.songsPath + '/' + data.song.id }>{data.song.name}</a>
        </td>
        <td>
          {data.song.tabs.length}
        </td>
      </tr>
    );
  }
}