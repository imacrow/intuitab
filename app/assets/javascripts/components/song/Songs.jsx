//dtHelper.initDataTables('#songs-table');
$(document).on('turbolinks:load', () => {
  if (!$(".dataTables_wrapper")[0]) {
    return $('#songs-table').DataTable();
  }
});

export default class Songs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      songs: this.props.data
    }
  }
  
  render() {
    const data = this.props;
    
    var songs = this.state.songs.map((song, index) => {
      return <Song    key         = {song.id}
                      song        = {song}
                      songsPath   = {data.songsPath} />
    });
    
    return (
      <div className='songs'>
        <h1 className='title'>Songs ({data.data.length})</h1>
        <table className='table table-striped' id='songs-table'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Total Tabs</th>
            </tr>
          </thead>
          <tbody>
            {songs}
          </tbody>
        </table>
      </div>
    );
  }
}

Songs.defaultProps = {
    songs: []
}; 