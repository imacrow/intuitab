import React from 'react';
import PropTypes from 'prop-types';

export default class Tab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: this.props.tab
    }
  }
  
  render () {
    const data = this.props;
    const tab = data.tab;

    return (
      <tr>
        <td>
          <a href={data.tabsPath + '/' + tab.id}>{tab.name}</a>
        </td>
        <td>
          <div className='star-rating' data-score={tab.average_rating}></div>
        </td>
        <td>
          {tab.created_at}
        </td>
        <td>
          <a href={data.usersPath + '/' + tab.user_id}>{tab.user_name}</a>
        </td>
      </tr>
    );
  }
}

Tab.propTypes = {
    tab: PropTypes.object.isRequired,
    usersPath: PropTypes.string.isRequired,
    tabsPath: PropTypes.string.isRequired
};