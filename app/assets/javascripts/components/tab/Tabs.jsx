function loadRaty() {
  $('.star-rating').raty({
    path: '/assets/',
    readOnly: true,
    space: false,
    score: function() {
        return $(this).attr('data-score');
    }
  })
}

$(document).on('turbolinks:load', () => {
  jQuery.fn.dataTableExt.oSort, {
    "rating-value-pre": (a) => parseFloat(a.match(/data-score="*(-?[0-9\.]+)/)[1]),
  
    "rating-value-asc": (a, b) => a > b,
  
    "rating-value-desc": (a, b) => a < b,
  };
 
  if (!$(".dataTables_wrapper")[0]) {
    $('#tabs-table')
      .on('init.dt', loadRaty())
      .on('draw.dt', loadRaty)
      .DataTable({
        "aoColumnDefs": [{
          "sType": "rating-value",
          "aTargets": [1]
        }]
    });
  }
});

export default class Tabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tabs: this.props.data
    }
  }
  
  render() {
    const data = this.props;
    
    var titleString = "Tabs";
    if (data.searchTerm) {
      titleString += ` containing \"${data.searchTerm}\"`
    }
    
    var tabs = this.state.tabs.map((tab, index) => {
      return <Tab     key        = {tab.id}
                      tab        = {tab}
                      tabsPath   = {data.tabsPath} 
                      usersPath  = {data.usersPath} />
    });
    
    return (
      <div className='tabs'>
        <h1 className='title'>{titleString} ({data.data.length})</h1>
        <table className='table table-striped' id='tabs-table'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Rating</th>
              <th>Date Created</th>
              <th>Created By</th>
            </tr>
          </thead>
          <tbody>
            {tabs}
          </tbody>
        </table>
      </div>
    );
  }
};