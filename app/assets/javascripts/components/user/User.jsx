export default class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user
    }
  }
  
  render () {
    const data = this.props;
    
    return (
      <tr>
        <td>
          <a href = { data.usersPath + '/' + data.user.id }>{data.user.name}</a>
        </td>
        <td>
          { data.user.tabs.length }
        </td>
      </tr>
    );
  }
}