// dtHelper.initDataTables('#users-table');
$(document).on('turbolinks:load', () => {
  if (!$(".dataTables_wrapper")[0]) {
    return $('#users-table').DataTable();
  }
});

export default class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: this.props.data
    }
  }
  
  render() {
    const data = this.props;
    
    var users = this.state.users.map((user, index) => {
      return <User    key         = {user.id}
                      user        = {user}
                      usersPath   = {data.usersPath} />
    });
    
    return (
      <div className='users'>
        <h1 className='title'>Users ({data.data.length})</h1>
        <table className='table table-striped' id='users-table'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Total Tabs Submitted</th>
            </tr>
          </thead>
          <tbody>
            {users}
          </tbody>
        </table>
      </div>
    );
  }
}

Users.defaultProps = {
    users: []
}; 
  