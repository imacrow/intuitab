export default function initDT(table) {
  $(document).on('turbolinks:load', () => {
    if (!$(".dataTables_wrapper")[0]) {
      return $(table).DataTable();
    }
  });
}
