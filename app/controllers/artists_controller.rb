class ArtistsController < ApplicationController
  def index
    @artists = Artist.all
  end
  
  def show
    @artist = Artist.find(params[:id])
    # need to get all albums, songs, and tabs
    @albums = @artist.albums.all
    # get every song from each album
    @songs = []
    @albums.each do |album|
      @songs += album.songs
    end
    # get every tab from each song
    @tabs = []
    @songs.each do |song|
      @tabs += song.tabs
    end
  end
end
