class RatingsController < ApplicationController
  before_action :set_tab
  before_action :logged_in_user, only: [:create, :update, :destroy]
  before_action :correct_user,   only: [:update, :destroy]
  before_action :valid_user,     only: :create
  
  def create
    @rating = @tab.ratings.create!(rating_params)
    flash[:success] = "Tab rated!"
    respond_to do |format|
      format.html { redirect_to @tab }
      format.js
    end
  end
  
  def update
    if @rating.update_attributes(rating_params)
      flash[:success] = "Tab rated!"
      redirect_to @tab
    end
  end
  
  def destroy
    @rating.destroy
    flash[:success] = "Rating cleared!"
    redirect_to @tab
  end
    
  private
  
    def set_tab
      @tab = Tab.find(params[:tab_id])
    end
    
    def rating_params
      params.require(:rating).permit(:value).merge(user_id: current_user.id)
    end
    
    # rating being updated or destroyed must belong to user 
    def correct_user
      @rating = current_user.ratings.find_by(id: params[:id])
      redirect_to root_url if @rating.nil?
    end
    
    # user cannot rate their own tab
    def valid_user
      if current_user.tabs.include?(@tab)
        flash[:danger] = "You cannot rate your own tab!"
        redirect_to root_url 
      end
    end
end
