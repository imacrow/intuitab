class SongsController < ApplicationController
  def index 
    @songs = Song.all
  end
  
  def show
    @song = Song.find(params[:id])
    @tabs = @song.tabs.all
  end
end
