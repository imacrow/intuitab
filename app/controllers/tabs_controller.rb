class TabsController < ApplicationController
  before_action :logged_in_user, only: [:create, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update, :destroy]
  
  def new
    @tab = Tab.new
  end
  
  def index
    if params[:search]
      @tabs = Tab.search(params[:search])
    else 
      @tabs = Tab
    end
    @tabs = tabViewQuery(@tabs)
  end
  
  def show
    @tab = Tab.find(params[:id])
  end
  
  def create
    @tab = Tab.new(user_params)
    if @tab.save
      flash.now[:success] = "Tab created!"
      redirect_to @tab
    else 
      render 'new'
    end
  end

  def edit
    @tab = Tab.find(params[:id])
  end
  
  def update
    @tab = Tab.find(params[:id])
    if @tab.update_attributes(tab_params)
      flash[:success] = "Tab updated!"
      redirect_to @tab
    else
      render 'edit'
    end
  end
  
  def destroy
    @tab.destroy
    flash[:success] = "Tab deleted!"
    redirect_to request.referrer || root_url
  end
  
  private
  
    def tab_params
      params.require(:tab).permit(:content)
    end
  
    def correct_user
      @tab = current_user.tabs.find_by(id: params[:id])
      redirect_to root_url if @tab.nil?
    end
end
