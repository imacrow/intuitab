module TabsHelper
  
  def tabViewQuery(tabs) 
    # need to display average rating
    # don't want to do one query for each tab
    # so, make index return a table that includes avg rating as a column
    # also, include user name and user id
    # and include only necessary columns to save memory
    # (don't need to load stuff like tab content, user email, etc)
    selectQuery = 'tabs.id, '\
                  'tabs.name, '\
                  'tabs.created_at, '\
                  'avg(ratings.value) as average_rating, '\
                  'users.id as user_id, '\
                  'users.name as user_name'
    tabs.joins(:ratings, :user).select(selectQuery).group("id")
  end
  
  # returns an array of tabs as json
  # formats the date as mm/dd/yy optional
  def tabsAsJson(tabs, formatDate: true) 
    @tabs.map do |tab| 
      tabJson = tab.as_json
      if formatDate
        tabJson.merge!(created_at: (tab.created_at.to_date.strftime("%m/%d/%y")).as_json)
      end
      tab = tabJson
    end
  end
end
