class Album < ApplicationRecord
  belongs_to :artist
  
  # album needs songs to exist
  has_many :songs, dependent: :destroy
  
  validates :artist_id, presence: true
  validates :name,      presence: true
  validates :genre,     presence: true
end
