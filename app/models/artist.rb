class Artist < ApplicationRecord
  # artist cannot exist without albums
  has_many :albums, dependent: :destroy
  
  validates :name,    presence: true
end
