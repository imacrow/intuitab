class Rating < ApplicationRecord
  belongs_to :user
  belongs_to :tab
  
  validates :user_id, presence: true
  validates :tab_id,  presence: true, 
                      uniqueness: { scope: :user_id, message: "You've already rated this tab!" }
  
  # value must be present and between 1 and 5
  validates :value,   presence: true
  validates_inclusion_of :value, :in => 1..5
end
