class Song < ApplicationRecord
  belongs_to :album
  
  # when song is destroyed, the tabs are also destroyed
  has_many :tabs, dependent: :destroy
  
  validates :album_id,  presence: true
  validates :name,      presence: true
  validates :genre,     presence: true
end
