class Tab < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :song
  
  # when tab is destroyed, ratings are also destroyed
  has_many :ratings, dependent: :destroy
  
  validates :name,    presence: true
  validates :type,    presence: true
  validates :user_id, presence: true
  validates :song_id, presence: true
  validates :content, presence: true
  
  def self.search(search) 
    where("tabs.name LIKE ?", "%#{search}%")
  end
end
