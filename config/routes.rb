Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  root 'static_pages#home'
  
  get     '/about',   to: 'static_pages#about'
  
  get     '/signup',  to: 'users#new'
  post    '/signup',  to: 'users#create'
  
  get     '/login',   to: 'sessions#new'
  post    '/login',   to: 'sessions#create'
  delete  '/logout',  to: 'sessions#destroy'
  
  resources :users
  
  resources :tabs do
    resources :ratings, only: [:create, :update, :destroy]
  end
  
  resources :account_activations, only: [:edit]
  
  resources :password_resets,     only: [:new, :create, :edit, :update]
  
  # songs, albums, and artists will all be created from tab creation
  # therefore, we don't need a 'new' route
  resources :artists,             except: :new
  
  resources :songs,               except: :new
  
  resources :albums,              except: :new
end
