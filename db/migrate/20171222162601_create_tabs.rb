class CreateTabs < ActiveRecord::Migration[5.1]
  def change
    create_table :tabs do |t|
      t.text :content
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :tabs, [:user_id, :created_at]
  end
end
