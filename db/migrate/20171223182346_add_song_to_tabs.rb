class AddSongToTabs < ActiveRecord::Migration[5.1]
  def change
    add_reference :tabs, :song, foreign_key: true
  end
end
