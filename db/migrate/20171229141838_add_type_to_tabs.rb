class AddTypeToTabs < ActiveRecord::Migration[5.1]
  def change
    add_column :tabs, :type, :string
  end
end
