class AddIndexToRatingsUseridTabid < ActiveRecord::Migration[5.1]
  def change
    add_index :ratings, [:user_id, :tab_id], unique: true
  end
end
