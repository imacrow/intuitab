# create admin user
User.create!(name:  "Admin",
             email: "admin@intuitab.com",
             password:              "password",
             password_confirmation: "password",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)
             
# create lots of fake users
29.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@intuitab.com"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

firstFiveUsers = User.order(:created_at).take(5)
allUsers = User.all
# create some fake artists
genres = ["rock", "indie", "alternative", "pop", "country"]
5.times do
  artist = Artist.new
  artistName = Faker::Hipster.sentence(2, true).chomp('.').titleize
  artist.name = artistName
  
  artist.save!
  
  # give them some fake albums
  3.times do
    album = Album.new
    albumName = (Faker::Color.color_name + " " + Faker::Hipster.word).chomp('.').titleize
    albumGenre = genres.sample
    album.name = albumName
    album.genre = albumGenre
    
    artist.albums << album
    album.save!
    
    # give the album some fake songs
    9.times do
      song = Song.new
      songName = (Faker::Hipster.sentence((rand(3) + 1), true)).chomp('.').titleize
      song.name = songName
      song.genre = albumGenre
      song.album = album
      
      album.songs << song
      song.save!

      # give the song a tab, created by random users from the first 5 users
      tabContent = "This is a tab"
      tabName = songName + " Tab"
      tabType = "Tab"
      tab = Tab.new(content: tabContent, song: song, name: tabName, type: tabType)
      user = firstFiveUsers.sample
      user.tabs << tab
      
      tab.save!
      
      # have the tab rated by 5 random users
      ratingUsers = allUsers.sample(5)
      ratingUsers.each do |ratingUser|
        ratingUser.rate(tab, rand(5) + 1)
      end
    end
  end
end