require 'test_helper'

class RatingsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @tab = tabs(:batterytab)
    @rating = ratings(:batterytabrating)
    @otheruser = users(:bill)
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Rating.count' do
      post tab_ratings_path(@tab), params: { rating: { value: 5 } }
    end
    assert_redirected_to login_url
  end
  
  test "should redirect create when trying to rate own tab" do
    log_in_as(@otheruser)
    assert_no_difference 'Rating.count' do
      post tab_ratings_path(@tab), params: { rating: { value: 2 } }
    end
    assert_redirected_to root_url
  end
  
  test "should redirect update when not logged in" do
    patch tab_rating_path(@tab, @rating), params: { rating: { value: 4 } }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "should redirect update when logged in as wrong user" do
    log_in_as(@otheruser)
    patch tab_rating_path(@tab, @rating), params: { rating: { value: 4 } }
    assert_not flash.empty?
    assert_redirected_to root_url
  end
    
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Rating.count' do
      delete tab_rating_path(@tab, @rating)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as wrong user" do
    log_in_as(@otheruser)
    assert_no_difference 'Rating.count' do
      delete tab_rating_path(@tab, @rating)
    end
    assert_redirected_to root_url
  end
end
