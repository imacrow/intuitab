require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  test "should get home" do
    get root_url
    assert_response :success
    assert_select "title", "Intuitab"
  end
  
  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | Intuitab"
  end

end
