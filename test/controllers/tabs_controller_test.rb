require 'test_helper'

class TabsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @tab  = tabs(:batterytab)
    @nick = users(:nick)
    @bill = users(:bill)
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Tab.count' do
      post tabs_path, params: { tab: { content: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end
  
  test "should redirect edit when not logged in" do
    get edit_tab_path(@tab)
    assert_not flash.empty?
    assert_redirected_to login_url
  end
    
  test "should redirect edit when logged in as wrong user" do
    log_in_as(@nick)
    get edit_tab_path(@tab)
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when not logged in" do
    patch tab_path(@tab), params: { tab: { content: "Lorem ipsum" } }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "should redirect update when logged in as wrong user" do
    log_in_as(@nick)
    patch tab_path(@tab), params: { tab: { content: "Lorem ipsum" } }
    assert_not flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Tab.count' do
      delete tab_path(@tab)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when logged in as wrong user" do
    log_in_as(@nick)
    assert_no_difference 'Tab.count' do
      delete tab_path(@tab)
    end
    assert_redirected_to root_url
  end
end
