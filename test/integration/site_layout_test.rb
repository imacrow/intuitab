require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:nick)
  end
  
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    
    # standard links regardless of log in status
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", tabs_path
    assert_select "a[href=?]", about_path
    
    # check layout links for logged out user
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", user_path(@user), count: 0
    assert_select "a[href=?]", edit_user_path(@user), count: 0
    assert_select "a[href=?]", logout_path, count: 0
    
    # log in, check layout links for logged in user
    log_in_as(@user)
    follow_redirect!
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_path(@user)
    assert_select "a[href=?]", logout_path
  end
end
