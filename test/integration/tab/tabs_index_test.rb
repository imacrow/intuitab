require 'test_helper'

class TabsIndexTest < ActionDispatch::IntegrationTest
  # make sure index renders and has correct react class
  test "index" do
    get tabs_path
    assert_template 'tabs/index'
    assert_select "div[data-react-class=Tabs]"
  end
end