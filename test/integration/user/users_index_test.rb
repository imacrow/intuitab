require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin      = users(:nick)
    @non_admin  = users(:bill)
  end
  
  test "index" do
    # non-admin users cannot access list of users
    log_in_as(@non_admin)
    get users_path
    assert_redirected_to root_url
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
  end
end