require 'test_helper'

class AlbumTest < ActiveSupport::TestCase
  
  def setup
    @album = albums(:masterofpuppets)
  end
  
  test "should be valid" do
    assert @album.valid?
  end
  
  test "artist should be present" do
    @album.artist_id = nil
    assert_not @album.valid?
  end
  
  test "name should be present" do
    @album.name = "   "
    assert_not @album.valid?
  end
  
  test "genre should be present" do
    @album.genre = "   "
    assert_not @album.valid?
  end
end
