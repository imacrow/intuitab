require 'test_helper'

class ArtistTest < ActiveSupport::TestCase
  def setup
    @artist = artists(:metallica)
  end
  
  test "should be valid" do
    assert @artist.valid?
  end
  
  test "name should be present" do
    @artist.name = "   "
    assert_not @artist.valid?
  end
end
