require 'test_helper'

class RatingTest < ActiveSupport::TestCase
  def setup
    @rating = ratings(:batterytabrating)
  end
  
  test "should be valid" do
    assert @rating.valid?
  end
  
  test "user id should be present" do
    @rating.user_id = nil
    assert_not @rating.valid?
  end
  
  test "tab id should be present" do
    @rating.tab_id = nil
    assert_not @rating.valid?
  end
  
  test "value should be between 1 and 5" do
    @rating.value = 0
    assert_not @rating.valid?
    @rating.value = 6
    assert_not @rating.valid?
  end
end
