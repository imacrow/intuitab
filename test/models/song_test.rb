require 'test_helper'

class SongTest < ActiveSupport::TestCase
  
  def setup
    @song = songs(:battery)
  end
  
  test "should be valid" do
    assert @song.valid?
  end
  
  test "album should be present" do
    @song.album_id = nil
    assert_not @song.valid?
  end
  
  test "name should be present" do
    @song.name = "   "
    assert_not @song.valid?
  end
  
  test "genre should be present" do
    @song.genre = "   "
    assert_not @song.valid?
  end
end
