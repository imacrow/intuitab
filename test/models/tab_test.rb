require 'test_helper'

class TabTest < ActiveSupport::TestCase
  
  def setup
    @tab = tabs(:batterytab)
  end
  
  test "should be valid" do
    assert @tab.valid?
  end
  
  test "name should be present" do
    assert @tab.valid?
  end
  
  test "type should be present" do
    assert @tab.valid?
  end
  
  test "user id should be present" do
    @tab.user_id = nil
    assert_not @tab.valid?
  end
  
  test "song id should be present" do
    @tab.song_id = nil
    assert_not @tab.valid?
  end
  
  test "content should be present" do
    @tab.content = "   " 
    assert_not @tab.valid?
  end
  
  test "associated ratings should be destroyed" do
    @tab.save
    assert_difference 'Rating.count', -1 do
      @tab.destroy
    end
  end
end
